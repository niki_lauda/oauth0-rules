| ORDER | NAME | ENABLED | ID |
| ----- | ---- | ------- | --
0 | custom-rule | true | rule_92odavcy
1 | another-rule | false | rule_936davsa
. | . | . | . | .

| ORDER | NAME | ENABLED | ID |
| --------|---------|-------|
| 0 | custom-rule | true | rule_92odavcy |
| 1 | another-rule | false | rule_936davsa |